orders = []
while True:
    order = input()
    
    if order == "":
        break
    orders.append(order)

def part1():
    depth = 0 
    horizontal = 0

    for item in orders:
        quantity = int(item.split(" ")[1])
        order_type = item.split(" ")[0]
        
        if(order_type == "up"):
            depth -= quantity
            
        elif(order_type == "down"):
            depth += quantity
        
        elif(order_type == "forward"):
            horizontal += quantity
    return horizontal * depth
   
def part2():
    depth = 0 
    horizontal = 0
    aim = 0

    for item in orders:
        quantity = int(item.split(" ")[1])
        order_type = item.split(" ")[0]
        
        if(order_type == "up"):
            aim -= quantity
            
        elif(order_type == "down"):
            aim += quantity
        
        elif(order_type == "forward"):
            horizontal += quantity
            depth += (aim * quantity)

    return horizontal * depth

print(part1())
print(part2())