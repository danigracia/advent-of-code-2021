import numpy as np

numbers = []
while True:
    number = str(input())

    if number == "":
        break
    
    numbers.append(number)

newarr = []

for number in numbers:
    newarr.append( [ int(i) for i in number ] )

def part1():
    gamma = []
    epsilon = []
    for i in range(len(newarr[0])):
        columna = []
        for collection in newarr:
            columna.append(collection[i])
        
        ones = np.count_nonzero(columna)
        zeros = len(columna) - np.count_nonzero(columna)
        
        if(ones > zeros):
            gamma.append(1)
            epsilon.append(0)
        else:
            gamma.append(0)
            epsilon.append(1)
    
    return [
        str(gamma).replace(",", "").replace("[", "").replace("]", "").replace(" ", ""),
        str(epsilon).replace(",", "").replace("[", "").replace("]", "").replace(" ", "")
    ]

def part2():
    print(newarr)
    for fila in newarr:
        for n in fila:
            print(n)
        

part2()